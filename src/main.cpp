#include <Homie.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include "DHTesp.h"
#include "Adafruit_Si7021.h"
#include <FS.h>

// Use to change relay mode based on whether the relay is active high or active low
#define RELAY_ON LOW
#define RELAY_OFF HIGH

// Homie
// -----------------
#define firmwareName "iThermostat"
#define firmwareVersion "1.2.2"

HomieNode thermostatNode("thermostat", "Thermostat", "climate");

HomieSetting<const char*> sensorTypeSetting("sensorType", "Type of sensor to pull from (DHT or SI)");
HomieSetting<long> targetTemperatureSetting("targetTemperature", "Desired temperature in F, 4 digit long (last two digits converted to decimal)");
HomieSetting<const char*> modeSetting("mode", "Mode of thermostat (cool, heat, off)");
HomieSetting<const char*> fanModeSetting("fanMode", "Mode of fan (auto, on)");

// Temperature Sensor
// -----------------
#ifdef ESP32
#error Select ESP8266 board.
#endif

Adafruit_Si7021 sensor = Adafruit_Si7021();

#define DHT_SENSOR_PIN D1
DHTesp dht;

// Relay Pins
// -----------------
#define COOL_PIN D5
#define FAN_PIN D6
#define HEAT_PIN D7

// Intervals
// -----------------
struct interval_t {
  unsigned long interval;
  unsigned long previousTime;
};
interval_t sensorInterval { 30000UL, 0UL };
interval_t checkInterval { 60000UL, 0UL };      // Intervals in milliseconds to check temperature and update thermostat

// State Tracking
// -----------------
enum mode { Off, Cool, Heat };
enum fanMode { Auto, On };
struct states_t {
  enum mode mode;
  enum fanMode fanMode;
};
struct pins_t {
  bool cool;
  bool heat;
  bool fan;
};
states_t currentState   { Off, Auto };
pins_t currentPin       { false, false, false };
states_t commandedState { Off, Auto };

// Cycle Tracking
// -----------------
unsigned int cycleProgress = 9;             // Time in current cycle in aprox minutes - Initialized at 9 to run cycleGoal code immediately
unsigned int minCycleGoal = 10;             // Minimum time required to complete a cycle in aprox minutes

// Temperature Readings
// -----------------
int targetTemperature = 0;    // Should be stored * 100 and num/100.num%100 to print ===== Thermostat will keep +/- 1.5 F of this
int currentTemperature = 0;   // Should be stored * 100 and num/100.num%100 to print

bool modeValueConverter(String value) {
  if (value == "off") {
    commandedState.mode = Off;
  } else if (value == "cool") {
    commandedState.mode = Cool;
  } else if (value == "heat") {
    commandedState.mode = Heat;
  } else {
    return false;
  }

  return true;
}

bool fanModeValueConverter(String value) {
  if (value == "on") {
    commandedState.fanMode = On;
  } else if (value == "auto") {
    commandedState.fanMode = Auto;
  } else {
    return false;
  }

  return true;
}

bool modeHandler(const HomieRange& range, const String& value) {
  if (!modeValueConverter(value)) {
    return false;
  }

  thermostatNode.setProperty("mode").send(value);
  Homie.getLogger() << "Mode is " << value << endl;

  return true;
}

bool fanModeHandler(const HomieRange& range, const String& value) {
  if (!fanModeValueConverter(value)) {
    return false;
  }

  thermostatNode.setProperty("fanmode").send(value);
  Homie.getLogger() << "Fan Mode is " << value << endl;

  return true;
}

bool targetTemperatureHandler(const HomieRange& range, const String& value) {
  int iValue = value.toFloat() * 100;

  if (iValue > 8000 || iValue < 6000) {
    Homie.getLogger() << "Target Temperature Out Of Range: " << value << endl;
    return false;
  }

  targetTemperature = iValue;

  thermostatNode.setProperty("targettemperature").send(value);
  Homie.getLogger() << "Target Temperature is " << value << endl;

  return true;
}

void updateTemperature() {
  float humidity;
  float temperature;

  if (std::string(sensorTypeSetting.get()) == "DHT") {
    TempAndHumidity tempAndHumid = dht.getTempAndHumidity();
    humidity = tempAndHumid.humidity;
    temperature = tempAndHumid.temperature;

  } else if (std::string(sensorTypeSetting.get()) == "SI") {
    humidity = sensor.readHumidity();
    temperature = sensor.readTemperature();

  } else {
    Homie.getLogger() << "Error: Invalid Sensor Type '" << sensorTypeSetting.get() << "'" << endl;
    return;
  }

  float temperatureF = temperature * 1.8 + 32;
  currentTemperature = temperatureF * 100;

  Homie.getLogger() << "Humidity: " << humidity << "\tTemperature: " << temperature << "\tTemperatureF: "<< temperatureF << endl;

  thermostatNode.setProperty("humidity").send(String(humidity));
  thermostatNode.setProperty("temperaturec").send(String(temperature));
  thermostatNode.setProperty("temperaturef").send(String(temperatureF));
}

void updateThermostat() {

  if (currentState.mode == Cool && !currentPin.cool) {
    if (currentTemperature - targetTemperature >= 150) {
      digitalWrite(COOL_PIN, RELAY_ON);
      digitalWrite(FAN_PIN, RELAY_ON);
      currentPin.cool = true;
      currentPin.fan = true;
      cycleProgress = 0;
    }
  } else if (currentState.mode == Heat && !currentPin.heat) {
    if (targetTemperature - currentTemperature >= 150) {
      digitalWrite(HEAT_PIN, RELAY_ON);
      digitalWrite(FAN_PIN, RELAY_ON);
      currentPin.heat = true;
      currentPin.fan = true;
      cycleProgress = 0;
    }
  }

  if (commandedState.fanMode != currentState.fanMode) {
    if (commandedState.fanMode == On) {
      digitalWrite(FAN_PIN, RELAY_ON);
      currentPin.fan = true;
      currentState.fanMode = On;
    } else if (commandedState.fanMode == Auto) {
      currentState.fanMode = Auto;
      if (!currentPin.cool && !currentPin.heat) {
        digitalWrite(FAN_PIN, RELAY_OFF);
        currentPin.fan = false;
      }
    }
  }

  cycleProgress++;

  // Handles logic after minimum cycle time has elapsed
  // If current state mode is off, run as well (effectively removing cycles from mattering while off)
  boolean turnOff = false;
  if (cycleProgress >= minCycleGoal || currentState.mode == Off) {

    if (currentState.mode != commandedState.mode) {
      turnOff = true;
      currentState.mode = commandedState.mode;
      
    } else if (currentState.mode == Cool && targetTemperature - currentTemperature >= 150) {
      turnOff = true;

    } else if (currentState.mode == Heat && currentTemperature - targetTemperature >= 150) {
      turnOff = true;

    }

    if (turnOff) {
      digitalWrite(COOL_PIN, RELAY_OFF);
      digitalWrite(HEAT_PIN, RELAY_OFF);
      currentPin.cool = false;
      currentPin.heat = false;
      if (currentState.fanMode == Auto) {
        digitalWrite(FAN_PIN, RELAY_OFF);
        currentPin.fan = false;
      }

      cycleProgress = 0;
    }
  }


}

void loopHandler() {
  unsigned long currentTime = millis();

  if (currentTime - sensorInterval.previousTime >= sensorInterval.interval) {
    sensorInterval.previousTime = currentTime;
    updateTemperature();
  }

  if (currentTime - checkInterval.previousTime >= checkInterval.interval) {
    checkInterval.previousTime = currentTime;
    updateThermostat();
  }

}

void setup()
{
  Serial.begin(115200);

  Homie_setFirmware(firmwareName, firmwareVersion);
  thermostatNode.advertise("humidity").setName("Humidity").setDatatype("float").setUnit("%");
  thermostatNode.advertise("temperaturec").setName("TemperatureC").setDatatype("float").setUnit("°C");
  thermostatNode.advertise("temperaturef").setName("TemperatureF").setDatatype("float").setUnit("°F");

  thermostatNode.advertise("mode").setName("Mode").setDatatype("string").settable(modeHandler);
  thermostatNode.advertise("fanmode").setName("FanMode").setDatatype("string").settable(fanModeHandler);
  thermostatNode.advertise("targettemperature").setName("TargetTemperature").setDatatype("float").settable(targetTemperatureHandler);
  Homie.setLoopFunction(loopHandler);
  
  thermostatNode.setRunLoopDisconnected(true);

  Homie.setup();

  if (std::string(sensorTypeSetting.get()) == "SI")  {
    if (!sensor.begin()) {
      Homie.getLogger() << "Did not find Si7021 sensor!" << endl;
    }

    Homie.getLogger() << "Sensor Type: ";
    switch(sensor.getModel()) {
      case SI_Engineering_Samples:
        Homie.getLogger() << "SI Engineering Samples";
        break;
      case SI_7013:
        Homie.getLogger() << "Si7013";
        break;
      case SI_7020:
        Homie.getLogger() << "Si7020";
        break;
      case SI_7021:
        Homie.getLogger() << "Si7021";
        break;
      case SI_UNKNOWN:
        Homie.getLogger() << "Unknown";
        break;
    }
    Homie.getLogger() << endl;
    
    Homie.getLogger() << "Revision #: " << sensor.getRevision() << endl;
  } else {
    dht.setup(DHT_SENSOR_PIN, DHTesp::DHT11);
  }

  targetTemperatureSetting.setValidator([] (long candidate) {
    return candidate <= 8000 && candidate >= 6000;
  });
  modeSetting.setValidator([] (const char* candidate) {
    return (String)candidate == "cool" || (String)candidate == "heat" || (String)candidate == "off";
  });
  fanModeSetting.setValidator([] (const char* candidate) {
    return (String)candidate == "auto" || (String)candidate == "on";
  });

  targetTemperature = targetTemperatureSetting.get();
  modeValueConverter(String(modeSetting.get()));
  fanModeValueConverter(String(fanModeSetting.get()));

  pinMode(COOL_PIN, OUTPUT);
  digitalWrite(COOL_PIN, RELAY_OFF);
  pinMode(FAN_PIN, OUTPUT);
  digitalWrite(FAN_PIN, RELAY_OFF);
  pinMode(HEAT_PIN, OUTPUT);
  digitalWrite(HEAT_PIN, RELAY_OFF);
}

void loop()
{
  Homie.loop();
}