# iThermostat
Please don't sue me Apple. It's just a joke. None of my other IoT projects start with I, I swear. Don't search for iLCD or iWash in the future.

### What?
Just some project I created to replace the Honeywell thermostat in my apartment. Who needs a smart thermostat when you can make your own dumb thermostat?

### How?
Just took an ESP8266, found a framework called Homie that standardizes MQTT communication for IoT, and wired that baby up with a relay. Currently running with an SI7021 temperature/humidity sensor and a quad-channel relay.

The ESP8266 talks with my local MQTT Broker which also talks to my local Home Assistant instance. Using that with HVAC MQTT I can easily script profiles and so on whilst having a nice friendly UI. More importantly, a UI which I did not have to design myself.